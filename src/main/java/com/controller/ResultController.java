package com.controller;

import com.service.ResultService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class ResultController {

    private final ResultService service;

    public ResultController(ResultService service) {
        this.service = service;
    }

    @GetMapping("/")
    public String fill() {
        //     model.addAttribute("result", service.learn(conditions));
        return "index";
    }

    @PostMapping("/")
    public String learn(Model model, @RequestParam("conditions") List<Integer> conditions) {
        model.addAttribute("result", service.learn(conditions));
        List<Boolean> checked = new ArrayList<>();
        for (int i = 0; i <8 ; i++) {
            checked.add(false);
        }
        conditions.forEach(i->checked.set(i,true));
        model.addAttribute("checked", checked);
        return "index";
    }
}
