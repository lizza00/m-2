package com.service;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ResultService {
    private final static String HEADPHONES = "Headphones";
    private final static String KEYBOARD = "Keyboard";
    private final static String BAG = "Bag";
    private final static String COSMETICS = "Cosmetics";

    public List<String> learn(List<Integer> conditions) {
        boolean quality = conditions.contains(0);
        boolean price = conditions.contains(1);
        boolean delivery = conditions.contains(2);
        boolean size = conditions.contains(3);
        boolean attractivity = conditions.contains(4);
        boolean comfort = conditions.contains(5);
        boolean convinience = conditions.contains(6);
        return resolve(quality, price, delivery, size,
                attractivity, comfort, convinience);
    }

    private List<String> resolve(boolean quality, boolean price,
                                 boolean delivery, boolean size,
                                 boolean attractivity, boolean comfort,
                                 boolean convinience) {
        ArrayList<String> result = new ArrayList<>();
// worksheet1.lgt
        {
// 1
            if ((!(quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    (!(size)) &&
                    (!(attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(HEADPHONES);

            }
// 2
            if (((quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    (!(size)) &&
                    (!(attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(KEYBOARD);

            }
// 3
            if ((!(quality)) &&
                    ((price)) &&
                    (!(delivery)) &&
                    (!(size)) &&
                    (!(attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(KEYBOARD);
                result.add(BAG);

            }
// 4
            if (((quality)) &&
                    ((price)) &&
                    (!(delivery)) &&
                    (!(size)) &&
                    (!(attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(BAG);

            }
// 5
            if ((!(quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    (!(attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(KEYBOARD);
                result.add(BAG);

            }
// 6
            if (((quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    (!(attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(COSMETICS);

            }
// 7
            if ((!(quality)) &&
                    ((price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    (!(attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(COSMETICS);

            }
// 8
            if (((quality)) &&
                    ((price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    (!(attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(HEADPHONES);
                result.add(BAG);

            }
// 9
            if ((!(quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(KEYBOARD);

            }
// 10
            if (((quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(HEADPHONES);

            }
// 11
            if ((!(quality)) &&
                    ((price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(BAG);

            }
// 12
            if (((quality)) &&
                    ((price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(KEYBOARD);
                result.add(BAG);

            }
// 13
            if ((!(quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(KEYBOARD);

            }
// 14
            if (((quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(KEYBOARD);

            }
// 15
            if ((!(quality)) &&
                    ((price)) &&
                    ((delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(KEYBOARD);
                result.add(COSMETICS);

            }
// 16
            if (((quality)) &&
                    ((price)) &&
                    ((delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(COSMETICS);

            }
// 17
            if ((!(quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    (!(size)) &&
                    ((attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(KEYBOARD);
                result.add(COSMETICS);

            }
// 18

// 19
            if ((!(quality)) &&
                    ((price)) &&
                    (!(delivery)) &&
                    (!(size)) &&
                    ((attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(KEYBOARD);
                result.add(BAG);

            }
// 20
            if (((quality)) &&
                    ((price)) &&
                    (!(delivery)) &&
                    (!(size)) &&
                    ((attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(BAG);

            }
// 21
            if ((!(quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    ((attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(BAG);

            }
// 22
            if (((quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    ((attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(KEYBOARD);

            }
// 23
            if ((!(quality)) &&
                    ((price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    ((attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(KEYBOARD);

            }
// 24
            if (((quality)) &&
                    ((price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    ((attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(KEYBOARD);

            }
// 25
            if ((!(quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(KEYBOARD);

            }
// 26
            if (((quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(KEYBOARD);
                result.add(BAG);

            }
// 27
            if ((!(quality)) &&
                    ((price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(BAG);
                result.add(COSMETICS);

            }
// 28
            if (((quality)) &&
                    ((price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(COSMETICS);

            }
// 29
            if ((!(quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(COSMETICS);

            }
// 30
            if (((quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(KEYBOARD);

            }
// 31
            if ((!(quality)) &&
                    ((price)) &&
                    ((delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(KEYBOARD);

            }
// 32
            if (((quality)) &&
                    ((price)) &&
                    ((delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    (!(comfort)) &&
                    (!(convinience))) {
                result.add(HEADPHONES);

            }
// 33
            if ((!(quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    (!(size)) &&
                    (!(attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(HEADPHONES);

            }
// 34
            if (((quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    (!(size)) &&
                    (!(attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(HEADPHONES);

            }
// 35
            if ((!(quality)) &&
                    ((price)) &&
                    (!(delivery)) &&
                    (!(size)) &&
                    (!(attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(HEADPHONES);
                result.add(BAG);

            }
// 36

// 37
            if ((!(quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    (!(attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(BAG);
                result.add(COSMETICS);

            }
// 38
            if (((quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    (!(attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(COSMETICS);

            }
// 39
            if ((!(quality)) &&
                    ((price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    (!(attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(COSMETICS);

            }
// 40
            if (((quality)) &&
                    ((price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    (!(attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(COSMETICS);

            }
// 41
            if ((!(quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(COSMETICS);

            }
// 42
            if (((quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(COSMETICS);

            }
// 43
            if ((!(quality)) &&
                    ((price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(BAG);

            }
// 44
            if (((quality)) &&
                    ((price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(BAG);
            }
// 45
            if ((!(quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(KEYBOARD);

            }
// 46
            if (((quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(HEADPHONES);

            }
// 47
            if ((!(quality)) &&
                    ((price)) &&
                    ((delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(BAG);
            }
// 48
            if (((quality)) &&
                    ((price)) &&
                    ((delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(COSMETICS);

            }
// 49
            if ((!(quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    (!(size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(KEYBOARD);

            }
// 50
            if (((quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    (!(size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(KEYBOARD);

            }
// 51
            if ((!(quality)) &&
                    ((price)) &&
                    (!(delivery)) &&
                    (!(size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(KEYBOARD);

            }
// 52
            if (((quality)) &&
                    ((price)) &&
                    (!(delivery)) &&
                    (!(size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(BAG);
            }
// 53
            if ((!(quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(COSMETICS);

            }
// 54
            if (((quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(HEADPHONES);
                result.add(COSMETICS);

            }
// 55
            if ((!(quality)) &&
                    ((price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(KEYBOARD);
                result.add(COSMETICS);

            }
// 56
            if (((quality)) &&
                    ((price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(KEYBOARD);
                result.add(COSMETICS);

            }
// 57
            if ((!(quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(KEYBOARD);
                result.add(BAG);
            }
// 58
            if (((quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(KEYBOARD);

            }
// 59
            if ((!(quality)) &&
                    ((price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(KEYBOARD);

            }
// 60
            if (((quality)) &&
                    ((price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(BAG);
            }
// 61
            if ((!(quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(KEYBOARD);

            }
// 62
            if (((quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(HEADPHONES);

            }
// 63
            if ((!(quality)) &&
                    ((price)) &&
                    ((delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(KEYBOARD);

            }
// 64
            if (((quality)) &&
                    ((price)) &&
                    ((delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    (!(convinience))) {
                result.add(BAG);
            }
// 65
            if ((!(quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    (!(size)) &&
                    (!(attractivity)) &&
                    (!(comfort)) &&
                    ((convinience))) {
                result.add(COSMETICS);

            }
// 66
            if (((quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    (!(size)) &&
                    (!(attractivity)) &&
                    (!(comfort)) &&
                    ((convinience))) {
                result.add(BAG);
                result.add(COSMETICS);

            }
// 67

// 68
            if (((quality)) &&
                    ((price)) &&
                    (!(delivery)) &&
                    (!(size)) &&
                    (!(attractivity)) &&
                    (!(comfort)) &&
                    ((convinience))) {
                result.add(BAG);
            }
// 69
            if ((!(quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    (!(attractivity)) &&
                    (!(comfort)) &&
                    ((convinience))) {
                result.add(KEYBOARD);

            }
// 70
            if (((quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    (!(attractivity)) &&
                    (!(comfort)) &&
                    ((convinience))) {
                result.add(HEADPHONES);

            }
// 71

// 72
            if (((quality)) &&
                    ((price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    (!(attractivity)) &&
                    (!(comfort)) &&
                    ((convinience))) {
                result.add(BAG);
            }
// 73
            if ((!(quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    (!(comfort)) &&
                    ((convinience))) {
                result.add(KEYBOARD);

            }
// 74
            if (((quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    (!(comfort)) &&
                    ((convinience))) {
                result.add(COSMETICS);

            }
// 75
            if ((!(quality)) &&
                    ((price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    (!(comfort)) &&
                    ((convinience))) {
                result.add(KEYBOARD);
            }
// 76
            if (((quality)) &&
                    ((price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    (!(comfort)) &&
                    ((convinience))) {
                result.add(BAG);
            }
// 77
            if ((!(quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    (!(comfort)) &&
                    ((convinience))) {
                result.add(BAG);
            }
// 78

// 79
            if ((!(quality)) &&
                    ((price)) &&
                    ((delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    (!(comfort)) &&
                    ((convinience))) {
                result.add(COSMETICS);

            }
// 80
            if (((quality)) &&
                    ((price)) &&
                    ((delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    (!(comfort)) &&
                    ((convinience))) {
                result.add(BAG);
            }
// 81
            if ((!(quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    (!(size)) &&
                    ((attractivity)) &&
                    (!(comfort)) &&
                    ((convinience))) {
                result.add(KEYBOARD);

            }
// 82
            if (((quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    (!(size)) &&
                    ((attractivity)) &&
                    (!(comfort)) &&
                    ((convinience))) {
                result.add(BAG);
            }
// 83
            if ((!(quality)) &&
                    ((price)) &&
                    (!(delivery)) &&
                    (!(size)) &&
                    ((attractivity)) &&
                    (!(comfort)) &&
                    ((convinience))) {
                result.add(KEYBOARD);

            }
// 84

// 85
            if ((!(quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    ((attractivity)) &&
                    (!(comfort)) &&
                    ((convinience))) {
                result.add(BAG);
            }
// 86
            if (((quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    ((attractivity)) &&
                    (!(comfort)) &&
                    ((convinience))) {
                result.add(BAG);
            }
// 87

// 88
            if (((quality)) &&
                    ((price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    ((attractivity)) &&
                    (!(comfort)) &&
                    ((convinience))) {
                result.add(BAG);
            }
// 89
            if ((!(quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    (!(comfort)) &&
                    ((convinience))) {
                result.add(KEYBOARD);
                result.add(COSMETICS);

            }
// 90

// 91
            if ((!(quality)) &&
                    ((price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    (!(comfort)) &&
                    ((convinience))) {
                result.add(BAG);
            }
// 92
            if (((quality)) &&
                    ((price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    (!(comfort)) &&
                    ((convinience))) {
                result.add(BAG);
            }
// 93
            if ((!(quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    (!(comfort)) &&
                    ((convinience))) {
                result.add(COSMETICS);

            }
// 94
            if (((quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    (!(comfort)) &&
                    ((convinience))) {
                result.add(KEYBOARD);
            }
// 95
            if ((!(quality)) &&
                    ((price)) &&
                    ((delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    (!(comfort)) &&
                    ((convinience))) {
                result.add(KEYBOARD);

            }
// 96
            if (((quality)) &&
                    ((price)) &&
                    ((delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    (!(comfort)) &&
                    ((convinience))) {
                result.add(KEYBOARD);

            }
// 97
            if ((!(quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    (!(size)) &&
                    (!(attractivity)) &&
                    ((comfort)) &&
                    ((convinience))) {
                result.add(BAG);
            }
// 98
            if (((quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    (!(size)) &&
                    (!(attractivity)) &&
                    ((comfort)) &&
                    ((convinience))) {
                result.add(BAG);
            }
// 99
            if ((!(quality)) &&
                    ((price)) &&
                    (!(delivery)) &&
                    (!(size)) &&
                    (!(attractivity)) &&
                    ((comfort)) &&
                    ((convinience))) {
                result.add(BAG);
            }
// 100

// 101
            if ((!(quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    (!(attractivity)) &&
                    ((comfort)) &&
                    ((convinience))) {
                result.add(HEADPHONES);

            }
// 102
            if (((quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    (!(attractivity)) &&
                    ((comfort)) &&
                    ((convinience))) {
                result.add(COSMETICS);

            }
// 103
            if ((!(quality)) &&
                    ((price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    (!(attractivity)) &&
                    ((comfort)) &&
                    ((convinience))) {
                result.add(KEYBOARD);

            }
// 104
            if (((quality)) &&
                    ((price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    (!(attractivity)) &&
                    ((comfort)) &&
                    ((convinience))) {
                result.add(KEYBOARD);
                result.add(COSMETICS);

            }
// 105
            if ((!(quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    ((comfort)) &&
                    ((convinience))) {
                result.add(HEADPHONES);
                result.add(KEYBOARD);
                result.add(BAG);
            }
// 106
            if (((quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    ((comfort)) &&
                    ((convinience))) {
                result.add(KEYBOARD);

            }
// 107
            if ((!(quality)) &&
                    ((price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    ((comfort)) &&
                    ((convinience))) {
                result.add(KEYBOARD);

            }
// 108
            if (((quality)) &&
                    ((price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    ((comfort)) &&
                    ((convinience))) {
                result.add(BAG);
            }
// 109
            if ((!(quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    ((comfort)) &&
                    ((convinience))) {
                result.add(KEYBOARD);

            }
// 110

// 111
            if ((!(quality)) &&
                    ((price)) &&
                    ((delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    ((comfort)) &&
                    ((convinience))) {
                result.add(BAG);
            }
// 112
            if (((quality)) &&
                    ((price)) &&
                    ((delivery)) &&
                    ((size)) &&
                    (!(attractivity)) &&
                    ((comfort)) &&
                    ((convinience))) {
                result.add(BAG);
            }
// 113
            if ((!(quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    (!(size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    ((convinience))) {
                result.add(BAG);
            }
// 114
            if (((quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    (!(size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    ((convinience))) {
                result.add(KEYBOARD);

            }
// 115

// 116
            if (((quality)) &&
                    ((price)) &&
                    (!(delivery)) &&
                    (!(size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    ((convinience))) {
                result.add(BAG);
            }
// 117
            if ((!(quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    ((convinience))) {
                result.add(COSMETICS);

            }
// 118
            if (((quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    ((convinience))) {
                result.add(BAG);
            }
// 119
            if ((!(quality)) &&
                    ((price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    ((convinience))) {
                result.add(BAG);
            }
// 120
            if (((quality)) &&
                    ((price)) &&
                    ((delivery)) &&
                    (!(size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    ((convinience))) {
                result.add(KEYBOARD);

            }
// 121
            if ((!(quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    ((convinience))) {
                result.add(BAG);
            }
// 122
            if (((quality)) &&
                    (!(price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    ((convinience))) {
                result.add(KEYBOARD);

            }
// 123
            if ((!(quality)) &&
                    ((price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    ((convinience))) {
                result.add(BAG);
            }
// 124
            if (((quality)) &&
                    ((price)) &&
                    (!(delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    ((convinience))) {
                result.add(KEYBOARD);

            }
// 125
            if ((!(quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    ((convinience))) {
                result.add(BAG);
            }
// 126
            if (((quality)) &&
                    (!(price)) &&
                    ((delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    ((convinience))) {
                result.add(HEADPHONES);

            }
// 127
            if ((!(quality)) &&
                    ((price)) &&
                    ((delivery)) &&
                    ((size)) &&
                    ((attractivity)) &&
                    ((comfort)) &&
                    ((convinience))) {
                result.add(KEYBOARD);
                result.add(BAG);

            }
// 128
        }

        return result;
    }
}
