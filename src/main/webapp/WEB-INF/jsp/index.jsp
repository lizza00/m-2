<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Lab 2</title>
    <link rel="stylesheet" href="bootstrap.css"/>
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <style>
        .checkbox {
            padding-left: 20px;
        }

        .checkbox-success input[type="checkbox"]:checked + label::before {
            background-color: #5cb85c;
            border-color: #5cb85c;
        }

        .checkbox-success input[type="checkbox"]:checked + label::after {
            color: #fff;
        }

        input[type="checkbox"].styled:checked + label:after {
            font-family: 'FontAwesome';
            content: "\f00c";
        }

        input[type="checkbox"] .styled:checked + label::before {
            color: #fff;
        }

        input[type="checkbox"] .styled:checked + label::after {
            color: #fff;
        }
    </style>
</head>
<body>
<div class="container" style="width: 100%">
    <nav class="navbar navbar-dark bg-primary" style="width: 100%">
        <div style="color: white">You need to make a decision?</div>
    </nav>
    <p>
        Present for friend</p>
    <form method="post">
        <section class="section-preview">
            <div class="col-md-4">
                <div class="checkbox checkbox-success">
                    <c:if test="${checked.get(0)}">
                        <input type="checkbox" name="conditions" class="styled" id="i1" value="0" checked></c:if>
                    <c:if test="${!checked.get(0)}"><input type="checkbox" name="conditions" class="styled" id="i1"
                                                           value="0"></c:if>
                    <label for="i1">Quality</label>
                </div>
                <div class="checkbox checkbox-success">
                    <c:if test="${checked.get(1)}">
                        <input type="checkbox" name="conditions" class="styled" id="i2" value="1" checked></c:if>
                    <c:if test="${!checked.get(1)}">
                        <input type="checkbox" name="conditions" class="styled" id="i2" value="1"></c:if>
                    <label for="i2">Price</label>
                </div>
                <div class="checkbox checkbox-success">
                    <c:if test="${checked.get(2)}">
                        <input type="checkbox" name="conditions" class="styled" id="i3" value="2" checked></c:if>
                    <c:if test="${!checked.get(2)}">
                        <input type="checkbox" name="conditions" class="styled" id="i3" value="2"></c:if>
                    <label for="i3">Delivery</label>
                </div>
                <div class="checkbox checkbox-success">
                    <c:if test="${checked.get(3)}">
                        <input type="checkbox" name="conditions" class="styled" id="i4" value="3" checked></c:if>
                    <c:if test="${!checked.get(3)}">
                        <input type="checkbox" name="conditions" class="styled" id="i4" value="3"></c:if>
                    <label for="i4">Size</label>
                </div>
                <div class="checkbox checkbox-success">
                    <c:if test="${checked.get(4)}">
                        <input type="checkbox" name="conditions" class="styled" id="i5" value="4" checked></c:if>
                    <c:if test="${!checked.get(4)}">
                        <input type="checkbox" name="conditions" class="styled" id="i5" value="4"></c:if>
                    <label for="i5">Attractivity</label>
                </div>
                <div class="checkbox checkbox-success">
                    <c:if test="${checked.get(5)}">
                        <input type="checkbox" name="conditions" class="styled" id="i6" value="5" checked></c:if>
                    <c:if test="${!checked.get(5)}">
                        <input type="checkbox" name="conditions" class="styled" id="i6" value="5"></c:if>
                    <label for="i6">Comfort</label>
                </div>
                <div class="checkbox checkbox-success">
                    <c:if test="${checked.get(6)}">
                        <input type="checkbox" name="conditions" class="styled" id="i7" value="6" checked></c:if>
                    <c:if test="${!checked.get(6)}">
                        <input type="checkbox" name="conditions" class="styled" id="i7" value="6"></c:if>
                    <label for="i7">Convinience</label>
                </div>
                <div class="checkbox checkbox-success">
                    <input type="hidden" name="conditions" class="styled" id="i8" value="7" checked>
                </div>

                <input class="btn btn-primary" type="submit" value="Learn">
            </div>
        </section>
    </form>
    <c:if test="${result!=null}">
        <div>Results:</div>
        <c:forEach items="${result}" var="r">
            <div style="margin: 10px; border-radius: 5%; background-color: #5cb85c; width: 200px; padding: 15px; text-align: center">${r}</div>
        </c:forEach></c:if>
</div>
</body>
</html>
